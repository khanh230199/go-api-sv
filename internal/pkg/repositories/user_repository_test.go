package repositories

import (
	"go-api-sv/internal/pkg/domains/models/entities"
	"go-api-sv/pkg/shared/database"
	"go-api-sv/pkg/shared/test"
	"regexp"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"gorm.io/gorm"
)

var mockUser = []entities.User{
	{
		BaseEntity: entities.BaseEntity{
			Model: gorm.Model{
				ID:        1,
				CreatedAt: time.Now(),
				UpdatedAt: time.Now(),
			},
		},
		// ID:       1,
		// CreatedAt: time.Now(),
		// UpdatedAt: time.Now(),
		// DeletedAt: nil,
		Username: "user1",
		Email:    "user1@email.com",
		Password: "$2a$10$5BfxC0mzu0oypxbZYpiXEeXPdJVXyXoM0CjwPN47WVfSvhlRQOr1u",
	},
	{
		BaseEntity: entities.BaseEntity{
			Model: gorm.Model{
				ID:        2,
				CreatedAt: time.Now(),
				UpdatedAt: time.Now(),
			},
		},
		// ID:        2,
		// CreatedAt: time.Now(),
		// UpdatedAt: time.Now(),
		// DeletedAt: nil,
		Username: "user2",
		Email:    "user2@email.com",
		Password: "$2a$10$5BfxC0mzu0oypxbZYpiXEeXPdJVXyXoM0CjwPN47WVfSvhlRQOr1u",
	},
}

func TestFind(t *testing.T) {
	mock, db := test.NewDBMock(database.PostgreSQL)
	rows := sqlmock.NewRows([]string{"id", "username", "email", "password", "created_at", "updated_at", "deleted_at"}).
		AddRow(mockUser[0].ID, mockUser[0].Username, mockUser[0].Email, mockUser[0].Password, mockUser[0].CreatedAt, mockUser[0].UpdatedAt, mockUser[0].DeletedAt).
		AddRow(mockUser[1].ID, mockUser[1].Username, mockUser[1].Email, mockUser[1].Password, mockUser[1].CreatedAt, mockUser[1].UpdatedAt, mockUser[1].DeletedAt)
	query := regexp.QuoteMeta(`SELECT * FROM "users" WHERE "users"."deleted_at" IS NULL`)
	mock.ExpectQuery(query).WillReturnRows(rows)
	repo := NewUserRepository(db)
	users, err := repo.Find()

	assert.NoError(t, err)
	assert.Len(t, users, 2)
	assert.Equal(t, mockUser, users)
}

func TestTakeByID(t *testing.T) {
	mock, db := test.NewDBMock(database.PostgreSQL)
	rows := sqlmock.NewRows([]string{"id", "username", "email", "password", "created_at", "updated_at", "deleted_at"}).
		AddRow(mockUser[0].ID, mockUser[0].Username, mockUser[0].Email, mockUser[0].Password, mockUser[0].CreatedAt, mockUser[0].UpdatedAt, mockUser[0].DeletedAt)
	userID := uint(1)
	query := regexp.QuoteMeta(`SELECT * FROM "users" WHERE "users"."id" = $1 AND "users"."deleted_at" IS NULL LIMIT 1`)
	mock.ExpectQuery(query).WillReturnRows(rows)
	repo := NewUserRepository(db)
	user, err := repo.TakeByID(userID)

	assert.NoError(t, err)
	assert.Equal(t, mockUser[0], user)
}

func TestTakeByUsername(t *testing.T) {

}

func TestTakeByEmail(t *testing.T) {

}

func TestCreate(t *testing.T) {

}
