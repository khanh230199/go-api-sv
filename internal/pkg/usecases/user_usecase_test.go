package usecases

import (
	"errors"
	"go-api-sv/internal/pkg/domains/interfaces/mocks"
	"go-api-sv/internal/pkg/domains/models/entities"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gorm.io/gorm"
)

var mockUser = []entities.User{
	{
		BaseEntity: entities.BaseEntity{
			Model: gorm.Model{
				ID:        1,
				CreatedAt: time.Now(),
				UpdatedAt: time.Now(),
			},
		},
		// ID:       1,
		// CreatedAt: time.Now(),
		// UpdatedAt: time.Now(),
		// DeletedAt: nil,
		Username: "user1",
		Email:    "user1@email.com",
		Password: "$2a$10$5BfxC0mzu0oypxbZYpiXEeXPdJVXyXoM0CjwPN47WVfSvhlRQOr1u",
	},
	{
		BaseEntity: entities.BaseEntity{
			Model: gorm.Model{
				ID:        2,
				CreatedAt: time.Now(),
				UpdatedAt: time.Now(),
			},
		},
		// ID:        2,
		// CreatedAt: time.Now(),
		// UpdatedAt: time.Now(),
		// DeletedAt: nil,
		Username: "user2",
		Email:    "user2@email.com",
		Password: "$2a$10$5BfxC0mzu0oypxbZYpiXEeXPdJVXyXoM0CjwPN47WVfSvhlRQOr1u",
	},
}

func TestFind(t *testing.T) {
	mockRepo := new(mocks.UserRepository)

	t.Run("Find success", func(t *testing.T) {
		mockRepo.On("Find", mock.Anything).Return(mockUser, nil).Once()
		usecase := NewUserUsecase(mockRepo)
		users, err := usecase.Find()

		assert.Equal(t, mockUser, users)
		assert.NoError(t, err)
	})

	t.Run("Find fail", func(t *testing.T) {
		mockRepo.On("Find", mock.Anything).Return([]entities.User{}, errors.New("Unexpexted Error")).Once()
		usecase := NewUserUsecase(mockRepo)
		users, err := usecase.Find()

		assert.Error(t, err)
		assert.Len(t, users, 0)
		assert.Empty(t, users)
	})
}

func TestTakeByID(t *testing.T) {
	mockRepo := new(mocks.UserRepository)

	t.Run("TakeByID success", func(t *testing.T) {
		mockRepo.On("TakeByID", mock.Anything).Return(mockUser[0], nil).Once()
		usecase := NewUserUsecase(mockRepo)
		user, err := usecase.TakeByID(1)

		assert.Equal(t, mockUser[0], user)
		assert.NoError(t, err)
	})

	t.Run("TakeByID fail", func(t *testing.T) {
		mockRepo.On("TakeByID", mock.Anything).Return(entities.User{}, gorm.ErrRecordNotFound).Once()
		usecase := NewUserUsecase(mockRepo)
		user, err := usecase.TakeByID(3)

		assert.Error(t, err)
		assert.Empty(t, user)
	})
}
